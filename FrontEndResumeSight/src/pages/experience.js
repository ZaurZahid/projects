import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import EducationPart from "../components/experience/education-part";
import JobPart from "../components/experience/job-part";
import ProjectPart from "../components/experience/project-part";

const useStyles = makeStyles(theme => ({
  buttonFont: {
    textTransform: "none"
  }
}));

const Experience = () => {
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column" alignItems="center" m={4}>
      <Grid container justify="center" spacing={2}>
        <Grid item xs={6} sm={4} md={2}>
          <Button
            variant="outlined"
            color="primary"
            fullWidth={true}
            size="large"
            className={classes.buttonFont}
          >
            Complete with Resume
          </Button>
        </Grid>
        <Grid item xs={6} sm={4} md={2}>
          <Button
            variant="outlined"
            color="primary"
            fullWidth={true}
            size="large"
            className={classes.buttonFont}
          >
            Complete with LinkedIn
          </Button>
        </Grid>
      </Grid>
      <EducationPart />
      <JobPart />
      <ProjectPart />
      <Grid container justify="center" spacing={3}>
        <Grid item xs={6} sm={4} md={2}>
          <Button
            variant="outlined"
            color="primary"
            fullWidth={true}
            size="large"
            className={classes.buttonFont}
          >
            Back
          </Button>
        </Grid>
        <Grid item xs={6} sm={4} md={2}>
          <Button
            variant="outlined"
            color="primary"
            fullWidth={true}
            size="large"
            className={classes.buttonFont}
          >
            Generate
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Experience;
