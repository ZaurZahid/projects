import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";

import featureSVG from "../assets/svg/feature.svg";

const useStyles = makeStyles(theme => ({
  largeSvg: {
    width: theme.spacing(32),
    height: theme.spacing(32)
  },
  buttonSpacing: {
    "& > *": {
      margin: theme.spacing(1)
    }
  },
  buttonFont: {
    textTransform: "none"
  }
}));

const SignUp = () => {
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column" alignItems="center" m={4}>
      <Typography variant="h5">
        Create your personal website in less than 2 minutes
      </Typography>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        m={2}
        className={classes.buttonSpacing}
      >
        <Avatar className={classes.largeSvg}>
          <img src={featureSVG} width="100%" alt="feature" />
        </Avatar>
        <Button
          variant="outlined"
          color="primary"
          fullWidth={true}
          size="large"
          className={classes.buttonFont}
        >
          Sign Up with LinkedIn
        </Button>
        <Button
          variant="outlined"
          color="primary"
          fullWidth={true}
          size="large"
          className={classes.buttonFont}
        >
          Sign Up by Email
        </Button>
        <Button
          variant="outlined"
          color="primary"
          fullWidth={true}
          size="large"
          className={classes.buttonFont}
        >
          Sign In
        </Button>
      </Box>
    </Box>
  );
};

export default SignUp;
