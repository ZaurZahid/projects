import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import "./carousel.css";

const useStyles = makeStyles(theme => ({
  buttonFont: {
    textTransform: "none"
  },
  buttonChoose: {
    textTransform: "none",
    marginTop: theme.spacing(1)
  },
  templateTitle: {
    marginBottom: theme.spacing(3)
  }
}));

const TemplateChoice = ({ cellCount }) => {
  const [curIndex, setCurIndex] = useState(0);
  const [carousel, setCarousel] = useState(null);

  useEffect(() => {
    setCarousel(document.querySelector(".carousel"));
  }, [carousel]);

  useEffect(() => {
    if (carousel) {
      rotateCarousel();
    }
  }, [curIndex]);

  const templateTitleByIndex = index => {
    let number;
    if (index >= 0) number = index % cellCount;
    else {
      number = index;
      while (number < 0) {
        number += cellCount;
      }
    }

    switch (number) {
      case 0:
        return "Consultant";
      case 1:
        return "Finance";
      case 2:
        return "Health";
      case 3:
        return "Manager";
      case 4:
        return "Teacher";
      case 5:
        return "Designer";
      default:
        return "Unknown";
    }
  };

  const rotateCarousel = () => {
    const angle = (360 / cellCount) * curIndex * -1;

    const cellWidth = carousel.offsetWidth;
    const radius = Math.round(cellWidth / 2 / Math.tan(Math.PI / cellCount));
    carousel.style.transform =
      "translateZ(" + -radius + "px) rotateY(" + angle + "deg)";
  };

  const handleNext = () => {
    setCurIndex(curIndex + 1);
  };

  const handlePrev = () => {
    setCurIndex(curIndex - 1);
  };

  const handleChoose = () => {};

  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column" alignItems="center" m={4}>
      <Typography variant="h5">Choose your professional template:</Typography>
      <div className="scene">
        <div className="carousel">
          <div className="carousel__cell" key="1" />
          <div className="carousel__cell" key="2" />
          <div className="carousel__cell" key="3" />
          <div className="carousel__cell" key="4" />
          <div className="carousel__cell" key="5" />
          <div className="carousel__cell" key="6" />
        </div>
      </div>
      <Typography variant="h6" className={classes.templateTitle}>
        {templateTitleByIndex(curIndex)}
      </Typography>
      <Grid container justify="center" spacing={3}>
        <Grid item xs={6} sm={3} md={2} lg={1}>
          <Button
            variant="outlined"
            color="primary"
            fullWidth={true}
            size="large"
            className={classes.buttonFont}
            onClick={handlePrev}
          >
            Back
          </Button>
        </Grid>
        <Grid item xs={6} sm={3} md={2} lg={1}>
          <Button
            variant="outlined"
            color="primary"
            fullWidth={true}
            size="large"
            className={classes.buttonFont}
            onClick={handleNext}
          >
            Next
          </Button>
        </Grid>
        <Grid container justify="center" spacing={4}>
          <Grid item xs={8} sm={4} md={3} lg={2}>
            <Button
              variant="outlined"
              color="primary"
              fullWidth={true}
              size="large"
              className={classes.buttonChoose}
              onClick={handleChoose}
            >
              Choose Template
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};
export default TemplateChoice;
