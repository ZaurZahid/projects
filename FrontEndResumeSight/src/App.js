import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import AppHeader from "./components/app-header";
import SignUp from "./pages/signup";
import TemplateChoice from "./pages/template-choice";
import Experience from "./pages/experience";

function App() {
  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/"
          component={() => (
            <React.Fragment>
              <AppHeader />
              <SignUp />
            </React.Fragment>
          )}
        />
        <Route
          path="/template"
          component={() => (
            <React.Fragment>
              <AppHeader />
              <TemplateChoice cellCount={6} />
            </React.Fragment>
          )}
        />
        <Route
          path="/experience"
          component={() => (
            <React.Fragment>
              <AppHeader />
              <Experience />
            </React.Fragment>
          )}
        />
      </Switch>
    </Router>
  );
}

export default App;
