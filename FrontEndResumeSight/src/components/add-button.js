import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import { Avatar } from "@material-ui/core";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";

const useStyles = makeStyles(theme => ({
  button: {
    color: "white",
    backgroundColor: "inherit"
  }
}));

const AddButton = () => {
  const classes = useStyles();
  return (
    <IconButton>
      <Avatar className={classes.button}>
        <AddCircleOutlineIcon />
      </Avatar>
    </IconButton>
  );
};

export default AddButton;
