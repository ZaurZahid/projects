import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import { pink } from "@material-ui/core/colors";
import { Avatar } from "@material-ui/core";
import AcUnitIcon from "@material-ui/icons/AcUnit";

const useStyles = makeStyles(theme => ({
  logo: {
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
    width: theme.spacing(8),
    height: theme.spacing(8)
  }
}));

const Logo = () => {
  const classes = useStyles();
  return (
    <Avatar className={classes.logo}>
      <AcUnitIcon />
    </Avatar>
  );
};

export default Logo;
