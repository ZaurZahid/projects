import React, { useReducer } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import AddButton from "../add-button";

import Job from "./job";

const initialState = {
  jobs: [{ title: "Title", description: "Description" }]
};

const addJob = (state, action) => {
  const newJobs = [...state.jobs];
  newJobs.push(action.job);
  return { jobs: newJobs };
};

const updateJob = (state, action) => {
  const newJobs = [...state.jobs];
  newJobs.map((job, index) => {
    if (index === action.index) {
      return action.job;
    }
    return job;
  });

  return { jobs: newJobs };
};

const rmJob = (state, action) => {
  const newJobs = [...state.jobs];
  newJobs.filter((job, index) => {
    if (index === action.index) return false;
    return true;
  });

  return { jobs: newJobs };
};

const reducer = (state, action) => {
  switch (action.type) {
    case "addJob":
      return addJob(state, action);
    case "updateJob":
      return updateJob(state, action);
    case "rmJob":
      return rmJob(state, action);
    default:
      throw new Error();
  }
};

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    },
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(48)
    },
    [theme.breakpoints.up("md")]: {
      width: theme.spacing(64)
    }
  },
  header: {
    backgroundColor: "#33BBFF",
    color: "white",
    textAlign: "left",
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(2)
  }
}));

const JobPart = () => {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Card variant="outlined" className={classes.card}>
      <CardHeader
        action={<AddButton />}
        title="Jobs"
        className={classes.header}
      />
      <CardContent>
        {state.jobs.map((job, index) => (
          <Job job={job} key={index} />
        ))}
      </CardContent>
    </Card>
  );
};

export default JobPart;
