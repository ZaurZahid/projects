import React, { useReducer } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import AddButton from "../add-button";

import Education from "./education";

const initialState = {
  educations: [{ title: "Title", description: "Description" }]
};

const addEdu = (state, action) => {
  const newEducations = [...state.educations];
  newEducations.push(action.education);
  return { educations: newEducations };
};

const updateEdu = (state, action) => {
  const newEducations = [...state.educations];
  newEducations.map((education, index) => {
    if (index === action.index) {
      return action.education;
    }
    return education;
  });

  return { educations: newEducations };
};

const rmEdu = (state, action) => {
  const newEducations = [...state.educations];
  newEducations.filter((education, index) => {
    if (index === action.index) return false;
    return true;
  });

  return { educations: newEducations };
};

const reducer = (state, action) => {
  switch (action.type) {
    case "addEdu":
      return addEdu(state, action);
    case "updateEdu":
      return updateEdu(state, action);
    case "rmEdu":
      return rmEdu(state, action);
    default:
      throw new Error();
  }
};

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    },
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(48)
    },
    [theme.breakpoints.up("md")]: {
      width: theme.spacing(64)
    }
  },
  header: {
    backgroundColor: "#33BBFF",
    color: "white",
    textAlign: "left",
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(2)
  },
  action: {
    color: "white",
    backgroundColor: "inherit"
  }
}));

const EducationPart = () => {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Card variant="outlined" className={classes.card}>
      <CardHeader
        action={<AddButton />}
        title="Education"
        className={classes.header}
      />
      <CardContent>
        {state.educations.map((education, index) => (
          <Education education={education} key={index} />
        ))}
      </CardContent>
    </Card>
  );
};

export default EducationPart;
