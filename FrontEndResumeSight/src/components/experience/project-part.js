import React, { useReducer } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import AddButton from "../add-button";

import Project from "./project";

const initialState = {
  projects: [{ title: "Title", description: "Description" }]
};

const addProj = (state, action) => {
  const newProjects = [...state.projects];
  newProjects.push(action.project);
  return { projects: newProjects };
};

const updateProj = (state, action) => {
  const newProjects = [...state.projects];
  newProjects.map((project, index) => {
    if (index === action.index) {
      return action.project;
    }
    return project;
  });

  return { projects: newProjects };
};

const rmProj = (state, action) => {
  const newProjects = [...state.projects];
  newProjects.filter((project, index) => {
    if (index === action.index) return false;
    return true;
  });

  return { projects: newProjects };
};

const reducer = (state, action) => {
  switch (action.type) {
    case "addProj":
      return addProj(state, action);
    case "updateProj":
      return updateProj(state, action);
    case "rmProj":
      return rmProj(state, action);
    default:
      throw new Error();
  }
};

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    },
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(48)
    },
    [theme.breakpoints.up("md")]: {
      width: theme.spacing(64)
    }
  },
  header: {
    backgroundColor: "#33BBFF",
    color: "white",
    textAlign: "left",
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(2)
  }
}));

const ProjectPart = () => {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Card variant="outlined" className={classes.card}>
      <CardHeader
        action={<AddButton />}
        title="Projects"
        className={classes.header}
      />
      <CardContent>
        {state.projects.map((project, index) => (
          <Project project={project} key={index} />
        ))}
      </CardContent>
    </Card>
  );
};

export default ProjectPart;
