import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles(theme => ({
  descPaper: {
    height: theme.spacing(8),
    paddingLeft: theme.spacing(1)
  },
  paper: {
    paddingLeft: theme.spacing(1)
  }
}));

const Education = ({ education }) => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Box pb={2}>
        <Paper variant="outlined" className={classes.paper}>
          <Typography
            variant="body1"
            color="textSecondary"
            component="p"
            align="left"
          >
            {education.title}
          </Typography>
        </Paper>
      </Box>
      <Box>
        <Paper variant="outlined" className={classes.descPaper}>
          <Typography
            variant="body1"
            color="textSecondary"
            component="p"
            align="left"
          >
            {education.description}
          </Typography>
        </Paper>
      </Box>
    </React.Fragment>
  );
};

export default Education;
