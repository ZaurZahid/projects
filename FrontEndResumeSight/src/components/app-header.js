import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import { pink } from "@material-ui/core/colors";
import { Avatar } from "@material-ui/core";
import AcUnitIcon from "@material-ui/icons/AcUnit";

const useStyles = makeStyles(theme => ({
  logo: {
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: "inherit",
    width: theme.spacing(6),
    height: theme.spacing(6)
  },
  colorPrimary: {
    padding: theme.spacing(1),
    backgroundColor: "#33BBFF"
  }
}));

const AppHeader = () => {
  const classes = useStyles();
  return (
    <AppBar position="sticky" className={classes.colorPrimary}>
      <Avatar className={classes.logo}>
        <AcUnitIcon />
      </Avatar>
    </AppBar>
  );
};

export default AppHeader;
